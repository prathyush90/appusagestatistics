    package prathyush.com.appusagestatistics;

    import android.app.AppOpsManager;

    import android.content.BroadcastReceiver;
    import android.content.Context;
    import android.content.Intent;
    import android.content.IntentFilter;
    import android.os.Build;
    import android.provider.Settings;
    import android.support.v4.app.LoaderManager;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.support.v7.widget.DefaultItemAnimator;
    import android.support.v7.widget.LinearLayoutManager;
    import android.support.v7.widget.RecyclerView;
    import android.util.Log;
    import android.widget.Toast;

    import java.util.ArrayList;
    import java.util.List;

    import prathyush.com.appusagestatistics.model.AppHistory;
    import prathyush.com.appusagestatistics.model.RecyclerViewAdapter;
    import prathyush.com.appusagestatistics.sqlhelpers.SqlAppHistoryGet;

    public class MainActivity extends AppCompatActivity implements onLoadInterface {

        private static final int USAGE_LOADER = 1;
        private UpdateReceiver mUpdateReceiver;
        private LoaderManager lm;
        private SqlAppHistoryGet sqlAppHistoryGet;
        private static MainActivity mInstance;
        private RecyclerView mRecyclerView;
        private RecyclerViewAdapter mAdapter;


        @Override
        protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);

            setContentView(R.layout.activity_main);
            mUpdateReceiver = new UpdateReceiver();
            mInstance = this;

            sqlAppHistoryGet = new SqlAppHistoryGet(getApplicationContext(), this);
            mRecyclerView = (RecyclerView) findViewById(R.id.list);
            mAdapter = new RecyclerViewAdapter(new ArrayList<AppHistory>(),this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapter);






        }

        /*
         This function checks if the user has granted app usage permission.
         He will be redirected to settings screen in case permission is required
         */

        public boolean checkPermissions()
        {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AppOpsManager appOps = (AppOpsManager) this
                        .getSystemService(Context.APP_OPS_SERVICE);
                int mode = 0;
                mode = appOps.checkOpNoThrow("android:get_usage_stats",
                        android.os.Process.myUid(), this.getPackageName());

                if (!(mode == AppOpsManager.MODE_ALLOWED)) {
                    Toast.makeText(MainActivity.this,"Please allow AppUsageStatistics app the permission to read usage statistics",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                    return false;

                }
            }
            return true;
        }
        /*
        Once the permission has been granted a service is started to collect the data and the
        loader manager initializes the loader for
        retrieving data from sql
         */

        public void postPermissions()
        {
            lm = getSupportLoaderManager();
            if (!UsageMonitorService.isRunning) {
                startService(new Intent(this, UsageMonitorService.class));
            }
            if (lm.getLoader( USAGE_LOADER) == null || !lm.getLoader( USAGE_LOADER).isStarted()) {
                lm.initLoader( USAGE_LOADER, null, sqlAppHistoryGet);
            }

        }

        @Override
        protected void onResume() {
            super.onResume();

            boolean b = checkPermissions();


            if(b)
            {
                postPermissions();
            }
        }



        @Override
        protected void onPause() {

            super.onPause();

        }

        @Override
        protected void onStop() {
            super.onStop();

        }

        @Override
        public void onAppHistory(List<AppHistory> appHistoryList) {

         mAdapter.dataChanged(appHistoryList);

        }

        public static class UpdateReceiver extends BroadcastReceiver {


            @Override
            public void onReceive(Context context, Intent intent) {

                if(mInstance != null) {
                    mInstance.restartLoader();
                }

            }
        }

        public void restartLoader()
        {

                lm.restartLoader( USAGE_LOADER, null, sqlAppHistoryGet);

        }


    }
