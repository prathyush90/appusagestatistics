package prathyush.com.appusagestatistics;

import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.support.v4.app.LoaderManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import prathyush.com.appusagestatistics.datasources.AppHistoryDataSource;
import prathyush.com.appusagestatistics.model.AppHistory;
import prathyush.com.appusagestatistics.sqlhelpers.DbHelper;
import prathyush.com.appusagestatistics.sqlhelpers.SqlAppHistoryGet;

public class UsageMonitorService extends Service  {

    private static Timer timer = new Timer();
    public static boolean isRunning    = false;
    private Context ctx;
    private long milliseconds = 0l;



    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        ctx = this;
        isRunning = true;
        milliseconds = System.currentTimeMillis()-120000;

        startService();
    }


/*
Starts a timer which runs for every hour to query app usage data
 */

    private void startService()
    {

        timer.scheduleAtFixedRate(new mainTask(), 0, 3600000);
        isRunning = true;
    }




    private class mainTask extends TimerTask
    {
        /*
        Runs every one hour ignores system apps usage statistics.
         */
        public void run()
        {
            final UsageStatsManager usageStatsManager = (UsageStatsManager) ctx.getSystemService(Context.USAGE_STATS_SERVICE);// Context.USAGE_STATS_SERVICE);
            long currMilliseconds = System.currentTimeMillis();
            List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, milliseconds, currMilliseconds);
            DbHelper helper = new DbHelper(ctx);
            SQLiteDatabase database = helper.getWritableDatabase();
            AppHistoryDataSource mDataSource = new AppHistoryDataSource(database);
            boolean entered = false;

            for(int i=0;i<queryUsageStats.size();i++)
            {
                try {
                    UsageStats usagestats = queryUsageStats.get(i);

                    ApplicationInfo applicationInfo = ctx.getPackageManager().getApplicationInfo(usagestats.getPackageName(), 0);
                    if(!isSystemPackage(applicationInfo)) {

                        entered = true;
                        AppHistory appHistory = new AppHistory();
                        appHistory.setPackagename(usagestats.getPackageName());
                        appHistory.setUsagetime(usagestats.getTotalTimeInForeground());
                        appHistory.setName(ctx.getPackageManager().getApplicationLabel(applicationInfo).toString());
                        boolean value = mDataSource.update(appHistory);
                        if(!value)
                        {
                            mDataSource.insert(appHistory);
                        }





                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

            }

            if(entered)
            {
                Intent i = new Intent("updated_appusage_sql");
                ctx.sendBroadcast(i);
            }


        }
    }

    /*
    Returns whether the application is a system app
     */

    private boolean isSystemPackage(ApplicationInfo applicationInfo) {
        return ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        isRunning = false;

    }
}
