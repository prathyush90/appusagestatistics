package prathyush.com.appusagestatistics.datasources;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import prathyush.com.appusagestatistics.model.AppHistory;


/**
 * Created by prathyush on 11/07/16.
 */
public class AppHistoryDataSource extends DataSource<AppHistory> {

    public static final String TABLE_NAME = "apphistory";

    public static final String COLUMN_NAME_APP = "typeapp";
    public static final String COLUMN_NAME_PACKAGE_NAME = "packagename";
    public static final String COLUMN_NAME_DURATION = "duration";


    public static String[] getAllColumns() {
        return new String[] {COLUMN_NAME_APP,COLUMN_NAME_PACKAGE_NAME,COLUMN_NAME_DURATION};
    }


    // Database creation sql statement
    public static final String CREATE_COMMAND = "create table " + TABLE_NAME
            + "("
            + COLUMN_NAME_DURATION+" integer,"+COLUMN_NAME_APP+" text,"+COLUMN_NAME_PACKAGE_NAME+" text );";
    public AppHistoryDataSource(SQLiteDatabase database) {
        super(database);
    }

    @Override
    public boolean insert(AppHistory entity) {

        if (entity == null) {
            return false;
        }
        long result = mDatabase.insert(TABLE_NAME, null,
                generateContentValuesFromObject(entity));
        return result != -1;
    }

    private ContentValues generateContentValuesFromObject(AppHistory entity) {

        if (entity == null) {
            return null;
        }
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME_APP, entity.getName());
        values.put(COLUMN_NAME_PACKAGE_NAME, entity.getPackagename());
        values.put(COLUMN_NAME_DURATION, entity.getUsagetime());


        return values;

    }

    @Override
    public boolean delete(AppHistory entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.delete(TABLE_NAME,
                COLUMN_NAME_PACKAGE_NAME + " = " + entity.getPackagename(), null);
        return result != 0;
    }

    @Override
    public boolean update(AppHistory entity) {
        if (entity == null) {
            return false;
        }
        int result = mDatabase.update(TABLE_NAME,
                generateContentValuesFromObject(entity), COLUMN_NAME_PACKAGE_NAME + " = " +"\"" +entity.getPackagename()+"\"", null);
        return result != 0;
    }

    @Override
    public List<AppHistory> read() {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), null,
                null, null, null, null);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    @Override
    public List<AppHistory> read(String selection, String[] selectionArgs,
                           String groupBy, String having, String orderBy) {
        Cursor cursor = mDatabase.query(TABLE_NAME, getAllColumns(), selection,
                selectionArgs, groupBy, having, orderBy);
        List tests = new ArrayList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                tests.add(generateObjectFromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return tests;
    }

    public AppHistory generateObjectFromCursor(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        AppHistory test = new AppHistory();

        test.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_APP)));
        test.setPackagename(cursor.getString(cursor.getColumnIndex(COLUMN_NAME_PACKAGE_NAME)));
        test.setUsagetime(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_DURATION)));


        return test;
    }


}
