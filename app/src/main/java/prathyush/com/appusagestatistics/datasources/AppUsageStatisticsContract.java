package prathyush.com.appusagestatistics.datasources;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;

/**
 * Created by prathyush on 27/09/16.
 */
public final class AppUsageStatisticsContract {
    /**
     * The authority of the lentitems provider.
     */
    public static final String AUTHORITY = "prathyush.com.appusagestatistics.datasources";
    /**
     * The content URI for the top-level lentitems authority.
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    /**
     * Constants for the App History table of the AppUsage provider.
     */
    public static final class Items implements BaseColumns {
        /**
         * The content URI for this table.
         */
        public static final Uri CONTENT_URI =  Uri.withAppendedPath(AppUsageStatisticsContract.CONTENT_URI, "usage");
        /**
         * A projection of all columns in the items table.Type app for name of app
         */

        public static final String[] PROJECTION_ALL = {"typeapp", "packagename", "duration"};

    }

}
