package prathyush.com.appusagestatistics.model;

/**
 * Created by prathyush on 11/07/16.
 */
public class AppHistory {


    long usagetime;
    String name;
    String packagename;

    public long getUsagetime() {
        return usagetime;
    }

    public void setUsagetime(long usagetime) {
        this.usagetime = usagetime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackagename() {
        return packagename;
    }

    public void setPackagename(String packagename) {
        this.packagename = packagename;
    }
}
