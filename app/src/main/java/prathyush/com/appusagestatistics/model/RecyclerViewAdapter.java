package prathyush.com.appusagestatistics.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;


import prathyush.com.appusagestatistics.R;


/**
 * Created by prathyush on 12/07/16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private List<AppHistory> mAppHistory;

    private Context mContext;
    public RecyclerViewAdapter(List<AppHistory> appHistory,Context context) {

           this.mAppHistory = appHistory;
           this.mContext    = context;
    }

    public void dataChanged(List<AppHistory> appHistory)
    {
        this.mAppHistory = appHistory;
        notifyDataSetChanged();
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        AppHistory appHistory = mAppHistory.get(position);
        long time = appHistory.getUsagetime();

        long remainder  = time % 3600000;
        int hours       = (int) (time/3600000);
        int minutes     = (int) (remainder/60000);
        int seconds     = (int) ((remainder % 60000) / 1000);
        String durationString  = (hours !=0 ? String.valueOf(hours)+"h " : " ") +String.valueOf(minutes)+"m "+String.valueOf(seconds)+"s ";
        holder.duration.setText(durationString);
        holder.nameApp.setText(appHistory.getName());
        try {
            holder.icon.setImageDrawable(mContext.getPackageManager().getApplicationIcon(appHistory.getPackagename()));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onViewDetachedFromWindow(MyViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return mAppHistory.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView nameApp;
        private TextView duration;


        public MyViewHolder(View itemView) {
            super(itemView);


            icon  = (ImageView) itemView.findViewById(R.id.icon);
            nameApp  = (TextView) itemView.findViewById(R.id.packagename);
            duration  = (TextView) itemView.findViewById(R.id.duration);

        }
    }
}
