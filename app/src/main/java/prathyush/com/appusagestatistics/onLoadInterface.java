package prathyush.com.appusagestatistics;

import java.util.List;

import prathyush.com.appusagestatistics.model.AppHistory;


/**
 * Created by prathyush on 11/07/16.
 */
public interface onLoadInterface {


    public void onAppHistory(List<AppHistory> appHistoryList);

}
