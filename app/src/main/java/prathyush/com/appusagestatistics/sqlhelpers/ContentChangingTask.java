package prathyush.com.appusagestatistics.sqlhelpers;

/**
 * Created by prathyush on 11/07/16.
 */
import android.os.AsyncTask;
import android.support.v4.content.Loader;

public abstract class ContentChangingTask<T1, T2, T3> extends
        AsyncTask<T1, T2, T3> {
    private Loader<?> loader=null;
    public ContentChangingTask(Loader<?> loader) {
        this.loader=loader;
    }
    @Override
    protected void onPostExecute(T3 param) {
        loader.onContentChanged();
    }
}
