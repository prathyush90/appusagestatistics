package prathyush.com.appusagestatistics.sqlhelpers;

/**
 * Created by prathyush on 11/07/16.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import prathyush.com.appusagestatistics.datasources.AppHistoryDataSource;


public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "elementora.db";
    private static final int DATABASE_VERSION = 2;
    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(AppHistoryDataSource.CREATE_COMMAND);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + AppHistoryDataSource.TABLE_NAME);

        onCreate(db);
    }
}
