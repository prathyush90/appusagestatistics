package prathyush.com.appusagestatistics.sqlhelpers;

import android.content.Context;

import java.util.List;

import prathyush.com.appusagestatistics.datasources.DataSource;
import prathyush.com.appusagestatistics.model.AppHistory;


/**
 * Created by prathyush on 11/07/16.
 */
public class SqlAppHistoryLoader extends AbstractDataLoader<List<AppHistory>> {

    private DataSource<AppHistory> mDataSource;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mGroupBy;
    private String mHaving;
    private String mOrderBy;

    public SqlAppHistoryLoader(Context context, DataSource dataSource, String selection, String[] selectionArgs,
                               String groupBy, String having, String orderBy) {
        super(context);
        mDataSource = dataSource;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mGroupBy = groupBy;
        mHaving = having;
        mOrderBy = orderBy;
    }

    @Override
    protected List<AppHistory> buildList() {
        List<AppHistory> testList = mDataSource.read(mSelection, mSelectionArgs, mGroupBy, mHaving,
                mOrderBy);
        return testList;
    }

    public void insert(AppHistory entity) {
        new InsertTask(this).execute(entity);
    }

    public void update(AppHistory entity) {
        new UpdateTask(this).execute(entity);
    }

    public void delete(AppHistory entity) {
        new DeleteTask(this).execute(entity);
    }

    private class InsertTask extends ContentChangingTask<AppHistory, Void, Void> {
        InsertTask(SqlAppHistoryLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.insert(params[0]);
            return (null);
        }
    }

    private class UpdateTask extends ContentChangingTask<AppHistory, Void, Void> {
        UpdateTask(SqlAppHistoryLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.update(params[0]);
            return (null);
        }
    }

    private class DeleteTask extends ContentChangingTask<AppHistory, Void, Void> {
        DeleteTask(SqlAppHistoryLoader loader) {
            super(loader);
        }

        @Override
        protected Void doInBackground(AppHistory... params) {
            mDataSource.delete(params[0]);
            return (null);
        }
    }
}
